require 'sidekiq/web'

Rails.application.routes.draw do
  root 'home#index'

  get 'seguranca_trabalho/index', to: 'seguranca_trabalho#index', path: 'sesmt'
  match 'seguranca_trabalho/importar', to: 'seguranca_trabalho#importar', path: 'importar', via: [:get, :post]

  get 'servico/index', to: 'servico#index', path: 'servico'

  match 'seguranca_trabalho/relatorio_atestados', to: 'seguranca_trabalho#relatorio_atestados', path: 'sesmt/relatorio_atestados', via: [:get, :post]
  match 'seguranca_trabalho/relatorio_cids', to: 'seguranca_trabalho#relatorio_cids', path: 'sesmt/relatorio_cids', via: [:get, :post]
  match 'seguranca_trabalho/relatorio_abs', to: 'seguranca_trabalho#relatorio_abs', path: 'sesmt/relatorio_abs', via: [:get, :post]


  match 'seguranca_trabalho/faltas_legais_old', to: 'seguranca_trabalho#faltas_legais_old', path: 'sesmt/faltas_legais_old', via: [:get, :post]

  match 'servico/boleto', to: 'servico#boleto', path: 'servico/boleto', via: [:get, :post]

  mount Sidekiq::Web => '/sidekiq'
end
