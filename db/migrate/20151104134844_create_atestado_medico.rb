class CreateAtestadoMedico < ActiveRecord::Migration
  def change
    create_table :atestado_medicos do |t|
      t.integer :filial
      t.string :matricula
      t.string :login
      t.string :nome
      t.text :tipo
      t.string :cid
      t.text :descricao
      t.date :data_entrega
      t.date :data_atestado
      t.integer :qtd_dias
      t.string :qtd_horas
      t.date :data_retorno
      t.string :incluido
      t.date :data_cadastro
      t.string :gerente
      t.string :veracidade
      t.string :cargo
      t.date :data_admissao
      t.string :produto
      t.string :campanha
      t.string :centro_custo
      t.string :medico
      t.string :crm

      t.timestamps null: false
    end
  end
end
