# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151104134844) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "atestado_medicos", force: :cascade do |t|
    t.integer  "filial"
    t.string   "matricula"
    t.string   "login"
    t.string   "nome"
    t.text     "tipo"
    t.string   "cid"
    t.text     "descricao"
    t.date     "data_entrega"
    t.date     "data_atestado"
    t.integer  "qtd_dias"
    t.string   "qtd_horas"
    t.date     "data_retorno"
    t.string   "incluido"
    t.date     "data_cadastro"
    t.string   "gerente"
    t.string   "veracidade"
    t.string   "cargo"
    t.date     "data_admissao"
    t.string   "produto"
    t.string   "campanha"
    t.string   "centro_custo"
    t.string   "medico"
    t.string   "crm"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

end
