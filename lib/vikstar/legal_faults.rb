#encoding: utf-8

require 'spreadsheet'
require 'csv'
require 'roo'

module Vikstar
	class LegalFaults
		attr_accessor :array_chart, :array_period, :array_enrollment, :array_hours_statement, :array_legal_faults, :array_crm, :duplicate_records

		def initialize(params={})
			# FILTER = ["atestado","declaracao de horas", "acompanhamento", "certidao"]

			@array = params[:array] || []
			@array_cost_center = []
			@array_chart = {}

			@array_all = []
			@array_hours_statement = []
			@array_legal_faults = []

			@array_period = []
			@array_enrollment = []
			@array_crm = []
			@array_cid = []

			@file = params[:file]
			@duplicate_records = 0

			@from = Date.parse(params[:from],"%d/%m/%Y")
			@to = Date.parse(params[:to],"%d/%m/%Y")
			@range = @from..@to

			read_file
			remove_duplicates
			chart
			chart_period

			clear_all
		end

		def read_file
			@array = []
			if(@file)
				spreadsheet = open_spreadsheet(@file)

				(2..spreadsheet.last_row).each do |i|

					row = spreadsheet.row(i)

					# p row

					if row && row[7].present? && row[20].present?

						# Validação data de entrega
						if row[7].is_a? String
							d = Date.parse(row[7],"%d/%m/%Y")
						else
							d = row[7]
						end

						# Validação data do atestado
						if row[8].is_a? String
							date_of_certificate = Date.parse(row[8],"%d/%m/%Y")
						else
							date_of_certificate = row[8]
						end

						# Verificar se a data esta entre o especificado
						if @range === d
							type = row[4].strip.to_s.downcase
							cost_center = row[20].strip.to_s
							cid = treat_cid({cid: row[5]})
							cid = cid.present? ? cid : "-"
							description = row[6].present? ? row[6].strip.to_s : "-"
							enrollment = row[1].to_i
							date = d.strftime('%F')
							date_of_certificate = date_of_certificate.strftime('%F')
							qty_days = row[9].present? ? row[9].to_i : 0
							crm = row[22].present? ? row[22].to_s : 0

							if(cost_center.present? && cid.present?)
								@array << { id: "#{enrollment}-#{cid}-#{date}-#{date_of_certificate}", enrollment: enrollment, type: type, cid: cid, description: description, cost_center: cost_center, date: date, date_of_certificate: date_of_certificate, qty_days:  qty_days, crm: crm }
							end

							if(!@array_cost_center.include? cost_center)
								@array_cost_center << cost_center
								@array_chart[cost_center] = []
							end
							if cid.present?
								# Verificar se já existe a cid no array do gráfico
								if !@array_chart[cost_center].detect{ |i| i[0] == cid }
									@array_chart[cost_center] << [cid,0,0]
								end

								if !@array_all.detect{ |i| i[0] == cid }
									# CID, Qtd, Dias
									@array_all << [cid,0,0]
								end

								if !@array_period.detect{ |i| i[:date] == date }
									# Data, Qtd
									@array_period << { date: date, certificates: 0, hours_statement: 0, legal_faults: 0, accompaniment: 0, testimonial: 0 }
								end

								if !@array_enrollment.detect{ |i| i[:enrollment] == enrollment }
									@array_enrollment << {enrollment: enrollment, certificates: 0, days: 0, hours_statement: 0, hours: 0, legal_faults: 0, accompaniment: 0, testimonial: 0, cost_center: cost_center}
								end

								if !@array_crm.detect{ |i| i[:crm] == crm }
									#if !verify_value({array: @array_crm, value: crm})
									# CRM, Qtd, Dias
									@array_crm << {crm: crm, certificates: 0, days: 0, hours_statement: 0, hours: 0, legal_faults: 0, accompaniment: 0, testimonial: 0 }
								end
							end
						end
					end
				end
				# É
				@file = @file.to_s.force_encoding('UTF-8')

				# p "----------------------------------------------------"
				# p @array_period
				# p "----------------------------------------------------"
			end
		end

		def remove_duplicates
			array_aux = @array.length

			# Remover os registros com ids duplicados
			@array = @array.uniq {|a| a[:id]}

			@duplicate_records = array_aux - @array.length
		end

		def treat_cid(params={})
			cid = params[:cid].to_s.strip
			array_verify = [".-",".0"]

			array_verify.each{ |a|
				if cid.include? a
					cid = cid.gsub(a.to_s,"")
				end
			}
			cid
		end

		def chart
			@array.each{ |a|

				@array_chart[a[:cost_center]].each{ |chart|
					if chart[0] == a[:cid] && a[:type] == "atestado"
						chart[1] += 1
						chart[2] += a[:qty_days]
					end
				}

				@array_all.each{ |value|
					if value[0] == a[:cid] && a[:type] == "atestado"
						value[1] += 1
						value[2] += a[:qty_days]
					end
				}

				@array_enrollment.each{ |value|
					if value[:enrollment] == a[:enrollment]
						case a[:type]
						when "atestado"
							value[:certificates] += 1
							value[:days] += a[:qty_days]
						when "declaracao de horas"
							value[:hours_statement] += 1
							value[:hours] += 3
						when "falta legal"
							value[:legal_faults] += 1
						when "acompanhamento"
							value[:accompaniment] += 1
						when "certidao"
							value[:testimonial] += 1
						end
					end
				}

				@array_crm.each{ |value|
					if value[:crm] == a[:crm]
						case a[:type]
						when "atestado"
							value[:certificates] += 1
							value[:days] += a[:qty_days]
						when "declaracao de horas"
							value[:hours_statement] += 1
							value[:hours] += 3
						when "falta legal"
							value[:legal_faults] += 1
						when "acompanhamento"
							value[:accompaniment] += 1
						when "certidao"
							value[:testimonial] += 1
						end
					end
				}

				case a[:type]
				when "declaracao de horas"
					@array_hours_statement << { enrollment: a[:enrollment], type: a[:type], cid: a[:cid], description: a[:description], cost_center: a[:cost_center], date: a[:date], date_of_certificate: a[:date_of_certificate], qty_days:  a[:qty_days], crm: a[:crm]}
				when "falta legal"
					@array_legal_faults << { enrollment: a[:enrollment], type: a[:type], cid: a[:cid], description: a[:description], cost_center: a[:cost_center], date: a[:date], date_of_certificate: a[:date_of_certificate], qty_days:  a[:qty_days], crm: a[:crm]}
				end
			}

			@array_chart.each{ |chart|
				# p chart[0]
				chart[1] = chart[1].sort_by{ |a| a[1] }
				# p chart[1]
			}

			#p @array_chart

			@array_chart['Todos'] = @array_all.sort_by { |a| a[1] }
		end

		def chart_period
			#@array = @array.sort_by { |a| a[:date] }
			@array_period = @array_period.sort_by { |a| a[:date] }
			@array.each{ |a|
				@array_period.each{ |chart|
					if chart[:date] == a[:date]
						case a[:type]
						when "atestado"
							chart[:certificates] += 1
						when "declaracao de horas"
							chart[:hours_statement] += 1
						when "falta legal"
							chart[:legal_faults] += 1
						when "acompanhamento"
							chart[:accompaniment] += 1
						when "certidao"
							chart[:testimonial] += 1
						end
					end
				}
			}

		end

		def clear_all
			@array_all = []
			@array = []
		end

		def open_spreadsheet(file)
			case File.extname(file.original_filename)
			when ".xls" then
				Roo::Spreadsheet.open(file.path, extension: :xls)
			when ".xlsx" then
				Roo::Spreadsheet.open(file.path, extension: :xlsx)
			else raise "Unknown file type: #{file.original_filename}"
			end
		end
	end
end
