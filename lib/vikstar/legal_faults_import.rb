#encoding: utf-8

# require 'spreadsheet'
# require 'csv'
# require 'roo'

require 'json'
require 'benchmark'
require 'csv'

module Vikstar
	class LegalFaultsImport
		attr_accessor :file, :data, :duplicate_records, :benchmark

		def initialize(params={})

			@file = params[:file]
			@data = []
			@duplicate_records = 0
			@benchmark = {
				start: nil,
				finish: nil
			}

			read_file
			remove_duplicates
		end

		def read_file
			@benchmark[:start] = Time.now
			if(@file)
				spreadsheet = open_spreadsheet(@file)

				if spreadsheet[:extension] != 'csv'
					(2..spreadsheet[:file].last_row).each do |i|
						mount_data(spreadsheet[:file].row(i))
					end
				else
					i = 0
					while line = spreadsheet[:file].readline
						if i > 0
							mount_data(line)
						end
						i += 1
					end
				end

				@file = @file.path
				spreadsheet = nil
			end
			@benchmark[:finish] = Time.now
		end

		def mount_data(row)
			if row && !row[0].blank? && !row[1].blank? && !row[2].blank? && !row[3].blank?
			#	p row
				cid = treat_cid({cid: row[5]})
				@data << {
					filial: row[0].to_i,
					matricula: row[1].to_s.strip,
					login: row[2].to_s.strip,
					nome: row[3].to_s.strip,
					tipo: row[4].to_s.strip.downcase,
					cid: cid.present? ? cid : '',
					descricao: row[6].present? ? row[6].strip.to_s : '',
					data_entrega: row[7].is_a?(String) ? Date.parse(row[7],"%d/%m/%Y") : row[7],
					data_atestado: row[8].is_a?(String) ? Date.parse(row[8],"%d/%m/%Y") : row[8],
					qtd_dias: row[9].present? ? row[9].to_i : 0,
					qtd_horas: row[10].to_s.strip,
					data_retorno: row[11].is_a?(String) ? Date.parse(row[11],"%d/%m/%Y") : row[11],
					incluido: row[12].to_s.strip,
					data_cadastro: row[13].is_a?(String) ? Date.parse(row[13],"%d/%m/%Y") : row[13],
					gerente: row[14].to_s.strip,
					veracidade: row[15].to_s.strip,
					cargo: row[16].to_s.strip,
					data_admissao: row[17].is_a?(String) ? Date.parse(row[17],"%d/%m/%Y") : row[17],
					produto: row[18].to_s.strip,
					campanha: row[19].to_s.strip,
					centro_custo: row[20].to_s.strip,
					medico: row[21].to_s.strip,
					crm: row[22].to_s.strip,
					status: '',
					id_aux: "#{row[1]}-#{row[5]}-#{row[7]}-#{row[8]}"
				}
			end
		end

		def remove_duplicates
			length_initial = @data.length

			# Remover os registros com ids duplicados
			@data.uniq! {|a| a[:id_aux]}

			@duplicate_records = length_initial - @data.length

		end

		def treat_cid(params={})
			params[:cid].to_s.strip
			array_verify = [".-",".0"]

			array_verify.each{ |a|
				if params[:cid] && params[:cid].include?(a)
					params[:cid].gsub(a.to_s,"")
				end
			}
			params[:cid]
		end

		def open_spreadsheet(file)
			case File.extname(file.original_filename)
			when ".xls" then
				{file: Roo::Spreadsheet.open(file.path, extension: :xls), extension: 'xls'}
			when ".xlsx" then
				{file: Roo::Spreadsheet.open(file.path, extension: :xlsx), extension: 'xlsx'}
			when ".csv" then
				{file: CSV.open(file.path), extension: 'csv'} #Roo::Spreadsheet.open(file.path, extension: :csv)
			else raise "Unknown file type: #{file.original_filename}"
			end
		end

		def measure(arg, &block)
		  no_gc = (arg == "no-gc")

		  if no_gc
		    GC.disable
		  else
		    # collect memory allocated during library loading
		    # and our own code before the measurement
		    GC.start
		  end

		  memory_before = `ps -o rss= -p #{Process.pid}`.to_i/1024
		  gc_stat_before = GC.stat
		  time = Benchmark.realtime do
		    yield
		  end
		  puts ObjectSpace.count_objects
		  unless no_gc
		    GC.start(full_mark: true, immediate_sweep: true, immediate_mark: false)
		  end
		  puts ObjectSpace.count_objects
		  gc_stat_after = GC.stat
		  memory_after = `ps -o rss= -p #{Process.pid}`.to_i/1024

		  puts({
		    RUBY_VERSION => {
		      gc: no_gc ? 'disabled' : 'enabled',
		      time: time.round(2),
		      gc_count: gc_stat_after[:count] - gc_stat_before[:count],
		      memory: "%d MB" % (memory_after - memory_before)
		    }
		  }.to_json)
		end

	end
end
