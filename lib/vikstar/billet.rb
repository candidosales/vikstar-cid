#encoding: utf-8

require 'spreadsheet'
require 'csv'
require 'roo'

module Vikstar
	class Billet
		attr_accessor :array_pdf, :array_excel, :array_audit, :file_pdf, :file_excel

		def initialize(params={})
			@array_pdf = []
			@array_excel = []
			@array_audit = []
			@array_cost_center = []
			@array_chart = []
			@array_values = {
			     total_difference: 0,
			     total_billet_pdf: 0,
			     qty_billet_pdf: 0,

			}
			@file_pdf = params[:file_pdf]
			@file_excel = params[:file_excel]

			@count_pdf = 0
			@count_excel = 0

			read_pdf_report
			read_excel_billet

			compare_data
			chart

			#clear_all
		end
		
		def read_pdf_report
		  p "Read pdf report ..."

		  if @file_pdf
		        reader = PDF::Reader.new(@file_pdf.path)
		        i = 0
                reader.pages.each do |page|
                	a = page.text.split(/\r?\n/)
	                a.each do |line|
	                    regex = /(?<date>\d{1,2}\/\d{1,2}\/\d{4})[\s:]*(?<billet>\d{10})[\s:]*(?<value>\d+(\.\d{2})?)\s*(?<car>\d+)/.match(line)
	                    if regex
	                    	billet = regex[:billet].to_s.gsub(/\A[0]{4,5}/,'')
	                    	value = '%.2f' % (regex[:value].to_f)
	                    	@array_pdf << {date: regex[:date], billet: billet.to_f, value: value, car: regex[:car]}
	                		i = i + 1

	                		# Calcular o total de todos dos boletos do PDF
	                		# p value
	                		# p @array_values[:total_billet_pdf]

	                		@array_values[:total_billet_pdf] += value.to_f
	                		@array_values[:qty_billet_pdf] += 1
	                	end
	                end
	            end
		  end
		  @count_pdf = i
		  @file_pdf = @file_pdf.to_s.force_encoding('UTF-8')
		end

		def read_excel_billet
			p "Read excel billet ..." 

			if @file_excel
				spreadsheet = open_spreadsheet(@file_excel)
				spreadsheet = spreadsheet.sheet(0)
				index = 0
				(2..spreadsheet.last_row).each do |i|
					row = spreadsheet.row(i)
					# Se existir valor
					if row[6]
						billet = row[2]
						value = '%.2f' % (row[6].to_f)

						cost_center = row[10].to_s

						@array_excel << {billet: billet, value: value, cost_center: cost_center}

						if(!@array_cost_center.include? cost_center)
					    	@array_cost_center << cost_center
					    	@array_chart << [cost_center,0]
					    end

						index += 1
					end	
			    end
			    @count_excel = index
			    @file_excel = @file_excel.to_s.force_encoding('UTF-8')
			else
				p "Nao leu o excel! -----------------------"
			end
			#@file_excel = ''
		end

		def compare_data
			p "Compare data ..."

			@array_audit = []
			if @array_pdf && @array_excel
				@array_pdf.each{ |pdf|
					exist_billet = false 
					@array_excel.each{ |excel| 
						if excel[:billet] == pdf[:billet] 
							exist_billet = true
							if excel[:value] != pdf[:value]

								# Calcular o total de todas as diferenças
								@array_values[:total_difference] = (excel[:value].to_f - pdf[:value].to_f ) + @array_values[:total_difference]

								@array_audit << {pdf_billet: pdf[:billet], pdf_value: pdf[:value], excel_billet: excel[:billet], excel_value: excel[:value], code: "different_value", message: "Valor diferente", type: "error" }
							end
                        end
					}
					if(!exist_billet)
						@array_audit << {pdf_billet: pdf[:billet], pdf_value: pdf[:value], code: "not_found", message: "Não encontrado na planilha ou valor não lançado", type: "error" }
				     else
						@array_audit << {pdf_billet: pdf[:billet], pdf_value: pdf[:value], code: "same_value", message: "Valores iguais", type: "success" }
					 end
				}
			end
		end

		def chart
			@array_excel.each{ |a|
				@array_chart.each{ |chart|
					if chart[0] == a[:cost_center]
						chart[1] += a[:value].to_f
					end
				}
			}
		end

		def clear_all
			@array_pdf = []
		end

		def open_spreadsheet(file)
		  case File.extname(file.original_filename)
		  when ".xls" then 
		  	Roo::Spreadsheet.open(file.path, extension: :xls)
		  when ".xlsx" then 
		  	Roo::Spreadsheet.open(file.path, extension: :xlsx)
		  else raise "Unknown file type: #{file.original_filename}"
		  end
		end
	end
end
