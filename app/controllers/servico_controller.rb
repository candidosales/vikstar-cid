#encoding: utf-8

class ServicoController < ApplicationController

  def index
  end

  def boleto
   @file_pdf = params[:file_pdf] || nil
   @file_excel = params[:file_excel] || nil
   p params
   if(params[:commit].present?)
     if(params[:file_pdf])
       @file_pdf = params[:file_pdf]
       @file_excel = params[:file_excel]

       @billet = Vikstar::Billet.new({file_pdf: @file_pdf, file_excel: @file_excel})
     else
       flash[:alert] = "Por favor, envie um arquivo do tipo PDF/Excel"
     end
   end
  end
end
