#encoding: utf-8

class SegurancaTrabalhoController < ApplicationController
  include Vikstar

  def index
  end

  def faltas_legais_old
    @file = params[:file] || nil
    if(params[:commit].present?)
      if(params[:file])
        @file = params[:file]
        @from = params[:from]
        @to = params[:to]

        @benchmark = {
          start: nil,
          finish: nil
        }
        @benchmark[:start] = Time.now
        @legal_faults = Vikstar::LegalFaults.new({file: @file, from: @from, to: @to})
        @benchmark[:finish] = Time.now

      else
        flash[:alert] = "Por favor, envie um arquivo do tipo Excel"
      end
    end
  end

  def relatorio_atestados
    if(params[:commit].present?)
      if(params[:daterange])
        @daterange = params[:daterange]
        periodo = params[:daterange].split(' - ')

        @from = DateTime.parse(periodo[0]).strftime('%Y-%m-%d')
        @to = DateTime.parse(periodo[1]).strftime('%Y-%m-%d')

        @benchmark = {
          start: nil,
          finish: nil
        }

        @benchmark[:start] = Time.now
        @atestados = AtestadoMedico.intervalo(@from,@to)
        @benchmark[:finish] = Time.now

      else
        flash[:alert] = "Por favor, envie um intervalo de datas válido"
      end
    end
  end

  def relatorio_abs
    if(params[:commit].present?)
      if(params[:ano])
        @ano = params[:ano]
        @atestado = AtestadoMedico
        @atestados_mensal_atestados = AtestadoMedico.abs_mensal_atestados(@ano)
        @atestados_mensal_area = AtestadoMedico.abs_mensal_area(@ano)
        @atestados_mensal_total_dias = AtestadoMedico.abs_mensal_total_dias(@ano).to_a

      else
        flash[:alert] = "Por favor, escolha um ano válido"
      end
    end
  end

  def importar
    @file = params[:file] || nil
    if(params[:commit].present?)
      if(params[:file])
        @file = params[:file]
        @legal_faults = {}
        @legal_faults = Vikstar::LegalFaultsImport.new({file: @file})
        @benchmark = {
          start: nil,
          finish: nil
        }
        @benchmark[:start] = Time.now
        @total_inserted = 0
        @total_errors = 0
        @legal_faults_registered = []
        @legal_faults_not_registered = []

        #measure('gc') do
        @legal_faults.data.each_with_index{ |a, i|
  				a.delete :id_aux
          a.delete :status

          atestado = AtestadoMedico.find_by(matricula: a[:matricula], cid: a[:cid], data_entrega: a[:data_entrega], data_atestado: a[:data_atestado])

          # atestado = AtestadoMedico.search(where: { matricula: a[:matricula], cid: a[:cid], data_entrega: a[:data_entrega], data_atestado: a[:data_atestado] } )

          if atestado.present?
            @legal_faults_registered << atestado
          else
            @legal_faults_not_registered << a
          end
  			}


        # too many terms in compound SELECT: INSERT INTO
        # inserts = []
        # @legal_faults_not_registered.each{ |atestado|
        #   inserts.push "(#{atestado[:filial]}, '#{atestado[:matricula]}', '#{atestado[:login]}', '#{atestado[:nome]}', '#{atestado[:tipo]}', '#{atestado[:cid]}', '#{atestado[:descricao]}', '#{atestado[:data_entrega]}', '#{atestado[:data_atestado]}', #{atestado[:qtd_dias]}, '#{atestado[:qtd_horas]}', '#{atestado[:data_retorno]}', '#{atestado[:incluido]}', '#{atestado[:data_cadastro]}', '#{atestado[:gerente]}', '#{atestado[:veracidade]}', '#{atestado[:cargo]}', '#{atestado[:data_admissao]}', '#{atestado[:produto]}', '#{atestado[:campanha]}', '#{atestado[:centro_custo]}', '#{atestado[:medico]}', '#{atestado[:crm]}', '#{DateTime.now.to_s}', '#{DateTime.now.to_s}')"
        # }
        #
        # values = inserts.join(' ,')
        #
        # sql = "INSERT INTO 'atestado_medicos' ( 'filial', 'matricula', 'login', 'nome', 'tipo', 'cid', 'descricao', 'data_entrega', 'data_atestado', 'qtd_dias', 'data_retorno', 'incluido', 'data_cadastro', 'gerente', 'veracidade', 'cargo', 'data_admissao', 'produto', 'campanha', 'centro_custo', 'medico', 'crm', 'created_at', 'updated_at') VALUES #{values}"


        ActiveRecord::Base.transaction {
          @legal_faults_not_registered.each{ |atestado|

            values = "(#{atestado[:filial]}, '#{atestado[:matricula]}', '#{atestado[:login]}', '#{atestado[:nome]}', '#{atestado[:tipo]}', '#{atestado[:cid]}', '#{atestado[:descricao]}', '#{atestado[:data_entrega]}', '#{atestado[:data_atestado]}', #{atestado[:qtd_dias]}, '#{atestado[:qtd_horas]}', '#{atestado[:data_retorno]}', '#{atestado[:incluido]}', '#{atestado[:data_cadastro]}', '#{atestado[:gerente]}', '#{atestado[:veracidade]}', '#{atestado[:cargo]}', '#{atestado[:data_admissao]}', '#{atestado[:produto]}', '#{atestado[:campanha]}', '#{atestado[:centro_custo]}', '#{atestado[:medico]}', '#{atestado[:crm]}', '#{DateTime.now.to_s}', '#{DateTime.now.to_s}')"

            ActiveRecord::Base.connection.execute "INSERT INTO atestado_medicos ( filial, matricula, login, nome, tipo, cid, descricao, data_entrega, data_atestado, qtd_dias, qtd_horas, data_retorno, incluido, data_cadastro, gerente, veracidade, cargo, data_admissao, produto, campanha, centro_custo, medico, crm, created_at, updated_at) VALUES #{values}"
          }
        }

        @total_inserted = @legal_faults_not_registered.count
#        end

        @benchmark[:finish] = Time.now
      else
        flash[:alert] = "Por favor, envie um arquivo do tipo Excel"
      end
    end
  end

  def relatorio_cids
    @matriculas = AtestadoMedico.distinct.order(:matricula).pluck(:matricula)
    @matriculas << 'Todas'
    @matricula = 'Todas'

    if params[:daterange].present?

      @matricula = params[:matricula]
      @qtd_ranking = params[:qtd_ranking]

      matricula_query = ''
      @daterange = params[:daterange]
      periodo = params[:daterange].split(' - ')

      @from = DateTime.parse(periodo[0]).strftime('%Y-%m-%d')
      @to = DateTime.parse(periodo[1]).strftime('%Y-%m-%d')

      if params[:matricula].present?
        if params[:matricula] != 'Todas'
          matricula_query = params[:matricula]
        end
      end

      if !matricula_query.blank?
        @atestados = AtestadoMedico.where(data_atestado: @from..@to, matricula: matricula_query).where.not(cid: '').order(data_atestado: :asc)
      else
        @atestados = AtestadoMedico.where(data_atestado: @from..@to).where.not(cid: '').order(data_atestado: :asc)
      end

      # http://blog.remarkablelabs.com/2012/12/not-equal-support-for-active-record-queries-rails-4-countdown-to-2013
    end
  end

end
