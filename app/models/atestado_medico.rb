class AtestadoMedico < ActiveRecord::Base
  searchkick callbacks: :async

  scope :intervalo, lambda { |from, to| where(data_atestado: from..to).order(data_atestado: :asc) }
  # scope :intervalo, lambda { |from, to| search(range: { data_entrega: { gte: from, lte: to } }, order: [{ data_entrega: {order: "asc"}}] ) }
  scope :abs_mensal_atestados, lambda{ |ano| where("DATE_TRUNC('year', data_atestado) = to_timestamp(?, 'YYYY')", ano).group(:tipo, "DATE_TRUNC('month',data_atestado)").order("DATE_TRUNC('month',data_atestado) ASC").count() }
  scope :abs_mensal_area, lambda{ |ano| where("DATE_TRUNC('year', data_atestado) = to_timestamp(?, 'YYYY')", ano).group(:centro_custo, "DATE_TRUNC('month',data_atestado)").order("DATE_TRUNC('month',data_atestado) ASC").count() }
  scope :abs_mensal_total_dias, lambda{ |ano| where("DATE_TRUNC('year', data_atestado) = to_timestamp(?, 'YYYY')", ano).group("DATE_TRUNC('month',data_atestado)").order("DATE_TRUNC('month',data_atestado) ASC").count(:qtd_dias) }

  def self.contagem_por_tipo(atestados)
    contagem = {
      atestado: 0,
      declaracao_de_horas: 0,
      falta_legal: 0,
      acompanhamento: 0,
      certidao: 0
    }

    atestados.each{ |atestado|
      case atestado[:tipo]
        when "atestado"
          contagem[:atestado] += 1
        when "declaracao de horas"
          contagem[:declaracao_de_horas] += 1
        when "falta legal"
          contagem[:falta_legal] += 1
        when "acompanhamento"
          contagem[:acompanhamento] += 1
        when "certidao"
          contagem[:certidao] += 1
        end
    }
    contagem
  end

  def self.contagem_por_matricula(atestados)
    all_aux = {}
    all_aux[:matriculas] = {}
    all_aux[:total_dias] = 0
    all_aux[:total_meses] = 0
    all_aux[:total_anos] = 0
    all_aux[:total_trabalhadores] = 0

    atestados.each{ |atestado|

      if all_aux[:matriculas][atestado[:matricula]].blank?
        all_aux[:matriculas][atestado[:matricula]] = { atestado: 0, declaracao_de_horas: 0, falta_legal: 0, acompanhamento: 0, certidao: 0, dias: 0, horas: 0, data_admissao: atestado[:data_admissao] }
      end

      case atestado[:tipo]
        when "atestado"
          all_aux[:matriculas][atestado[:matricula]][:atestado] += 1
          all_aux[:matriculas][atestado[:matricula]][:dias] += atestado[:qtd_dias]

          all_aux[:total_dias] += atestado[:qtd_dias]

        when "declaracao de horas"
          all_aux[:matriculas][atestado[:matricula]][:declaracao_de_horas] += 1
          all_aux[:matriculas][atestado[:matricula]][:horas] += 3
        when "falta legal"
          all_aux[:matriculas][atestado[:matricula]][:falta_legal] += 1
        when "acompanhamento"
          all_aux[:matriculas][atestado[:matricula]][:acompanhamento] += 1
        when "certidao"
          all_aux[:matriculas][atestado[:matricula]][:certidao] += 1
      end
    }
    # Considerando o mês 26 dias trabalhados
    all_aux[:total_meses] = all_aux[:total_dias] / 26
    all_aux[:total_anos] = all_aux[:total_meses] / 12

    # Considerando 6:20h = 380min / 24h = 1440min
    all_aux[:total_trabalhadores] = all_aux[:total_dias] * 1440 / 380

    all_aux

  end

  def self.contagem_por_crm(atestados)
    all_aux = {}
    atestados.each{ |atestado|

      if all_aux[atestado[:crm]].blank?
        all_aux[atestado[:crm]] = { atestado: 0, declaracao_de_horas: 0, falta_legal: 0, acompanhamento: 0, certidao: 0, dias: 0, horas: 0 }
      end

      case atestado[:tipo]
        when "atestado"
          all_aux[atestado[:crm]][:atestado] += 1
          all_aux[atestado[:crm]][:dias] += atestado[:qtd_dias]
        when "declaracao de horas"
          all_aux[atestado[:crm]][:declaracao_de_horas] += 1
          all_aux[atestado[:crm]][:horas] += 3
        when "falta legal"
          all_aux[atestado[:crm]][:falta_legal] += 1
        when "acompanhamento"
          all_aux[atestado[:crm]][:acompanhamento] += 1
        when "certidao"
          all_aux[atestado[:crm]][:certidao] += 1
      end
    }

    all_aux
  end

  def self.contagem_por_data_atestado(atestados)
    all_aux = {}

    atestados.each{ |atestado|
      if all_aux[atestado[:data_atestado]].blank?
        all_aux[atestado[:data_atestado]] = { atestado: 0, declaracao_de_horas: 0, falta_legal: 0, acompanhamento: 0, certidao: 0 }
      end

      case atestado[:tipo]
        when "atestado"
          all_aux[atestado[:data_atestado]][:atestado] += 1
        when "declaracao de horas"
          all_aux[atestado[:data_atestado]][:declaracao_de_horas] += 1
        when "falta legal"
          all_aux[atestado[:data_atestado]][:falta_legal] += 1
        when "acompanhamento"
          all_aux[atestado[:data_atestado]][:acompanhamento] += 1
        when "certidao"
          all_aux[atestado[:data_atestado]][:certidao] += 1
      end
    }

    all_aux
  end

  def self.contagem_cid_por_data_atestado(atestados)
    all_aux = {}

    atestados.each{ |atestado|

      if all_aux[atestado[:cid]].blank?
        all_aux[atestado[:cid]] = { }
      end

      if all_aux[atestado[:cid]][atestado[:data_atestado]].blank?
        all_aux[atestado[:cid]][atestado[:data_atestado]] = 0
      end

      all_aux[atestado[:cid]][atestado[:data_atestado]] += 1

    }

    all_aux
  end

  def self.contagem_cid(atestados)
    all_aux = {}

    atestados.each{ |atestado|
      if all_aux[atestado[:cid]].blank?
        all_aux[atestado[:cid]] = 0
      end

      all_aux[atestado[:cid]] += 1

    }
    all_aux.sort_by{ |key, value| value }.reverse
  end

  def self.contagem_cid_por_data_atestado(atestados)
    all_aux = {}

    atestados.each{ |atestado|
      if all_aux[atestado[:cid]].blank?
        all_aux[atestado[:cid]] = { }
      end

      if all_aux[atestado[:cid]][atestado[:data_atestado]].blank?
        all_aux[atestado[:cid]][atestado[:data_atestado]] = 0
      end

      all_aux[atestado[:cid]][atestado[:data_atestado]] += 1

    }

    all_aux
  end


  def self.contagem_cid_por_centro_custo(atestados)
    all_aux = {}
    atestados.each{ |atestado|
      if all_aux[atestado[:centro_custo]].blank?
        all_aux[atestado[:centro_custo]] = []
      end

      if !all_aux[atestado[:centro_custo]].detect{ |i| i[0] == atestado[:cid] }
        all_aux[atestado[:centro_custo]] << [atestado[:cid],0,0]
      end

      all_aux[atestado[:centro_custo]].each{ |a|
        if atestado[:tipo] == 'atestado' && atestado[:cid] == a[0]
          a[1] += 1
          a[2] += atestado[:qtd_dias]
        end
      }
    }

    all_aux
  end

  def self.contagem_por_tipo_atestado(atestados,tipo)
    all_aux = []
    atestados.each{ |atestado|
      if(atestado[:tipo] == tipo)
        all_aux << { matricula: atestado[:matricula], descricao: atestado[:descricao], dias: atestado[:qtd_dias] }
      end
    }
    all_aux
  end

  def self.construir_abs_mensal(data)
    all = {
      x_axis: [],
      series: []
    }

    data.each { |a|
      name = a[0][0].blank? ? 'sem titulo' : a[0][0]
      month = a[0][1].strftime('%m/%Y')
      qty = a[1]

      if !all[:x_axis].detect{ |i| i == month }
        all[:x_axis] << month
      end

      existName = false

      all[:series].each{ |i|
        if i[:name] == name
          i[:data] << qty
          existName = true
          break
        end
      }

      if !existName
        all[:series] << { name: name, data: [ qty ] }
      end
    }

    all
  end

  def self.construir_abs_mensal_area(data)
    all = {
      x_axis: [],
      series: []
    }

    data.each { |a|
      name = a[0][0].blank? ? 'sem titulo' : a[0][0]
      month = a[0][1].strftime('%m/%Y')
      qty = a[1]

      if !all[:x_axis].detect{ |i| i == month }
        all[:x_axis] << month
      end

      existName = false

      all[:series].each{ |i|
        if i[:name] == name
          i[:data] << qty
          existName = true
          break
        end
      }

      if !existName
        all[:series] << { name: name, data: [ qty ] }
      end
    }

    all
  end

  def self.construir_abs_mensal_dias(data)
    all = {
      x_axis: [],
      series: [{
        name: 'Total de dias',
        data: []
        }]
    }

    data.each { |a|
      month = a[0].strftime('%m/%Y')
      qty = a[1]

      if !all[:x_axis].detect{ |i| i == month }
        all[:x_axis] << month
      end

      existName = false

      all[:series].each{ |i|
        i[:data] << qty
      }

    }

    all
  end


end
