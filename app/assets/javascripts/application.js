// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ujs/src/rails
//= require vendor/bootstrap
//= require vendor/highstock
//= require vendor/exporting
//= require vendor/offline-exporting

//= require vendor/boost

//= require select2/dist/js/select2

//= require moment/moment
//= require moment/locale/pt-br
//= require bootstrap-daterangepicker/daterangepicker

//= require datatables/media/js/jquery.dataTables
//= require datatables-tabletools/js/dataTables.tableTools
//= require vendor/dataTables.bootstrap

//= require module/legalFaultsOld
//= require module/atestadoMedicos
//= require module/cids
//= require module/billet

Highcharts.theme = {
	colors: ['#F44336', '#9C27B0', '#3F51B5', '#03A9F4', '#009688', '#8BC34A', '#FDD835', '#FF9800', '#795548', '#607D8B', '#E91E63', '#673AB7', '#2196F3', '#00BCD4', '#4CAF50', '#CDDC39', '#FFC107', '#FF5722', '#9E9E9E'],
	title: {
		style: {
			color: '#000',
			font: 'bold 16px "Lato", Verdana, sans-serif'
		}
	},
	subtitle: {
		style: {
			color: '#666666',
			font: 'bold 12px "Lato", Verdana, sans-serif'
		}
	},
	legend: {
		itemStyle: {
			font: '9pt "Lato", Verdana, sans-serif',
			color: 'black'
			},
			itemHoverStyle: {
				color: 'gray'
					}
			}
	};

Highcharts.setOptions(Highcharts.theme);
Highcharts.setOptions({
	    lang: {
	        months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
	        shortMonths: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
	        weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
	        loading: ['Atualizando o gráfico...aguarde'],
	        contextButtonTitle: 'Exportar gráfico',
	        decimalPoint: ',',
	        thousandsSep: '.',
	        downloadJPEG: 'Baixar imagem JPEG',
	        downloadPDF: 'Baixar arquivo PDF',
	        downloadPNG: 'Baixar imagem PNG',
	        downloadSVG: 'Baixar vetor SVG',
	        printChart: 'Imprimir gráfico',
	        rangeSelectorFrom: 'De',
	        rangeSelectorTo: 'Para',
	        rangeSelectorZoom: 'Zoom',
	        resetZoom: 'Limpar Zoom',
	        resetZoomTitle: 'Voltar Zoom para nível 1:1',
	    }
	});
