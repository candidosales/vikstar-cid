# Vikstar Changelog

### Vikstar 1.6.4 (14 de Dezembro de 2015)
* Exportar gráficos offline, ok;
* Gráfico por porcentagem em ABS mensal, ok;
* Otimização de inserção de dados, ok;

// Excel
{"2.1.5":{"gc":"enabled","time":2.79,"gc_count":0,"memory":"557 MB"}} --> memory leak
{"2.1.5":{"gc":"disabled","time":6.32,"gc_count":0,"memory":"546 MB"}}
{"2.1.5":{"gc":"disabled","time":2.82,"gc_count":0,"memory":"629 MB"}}
{"2.1.5":{"gc":"enabled","time":2.61,"gc_count":0,"memory":"556 MB"}} --> memory leak
{"2.1.5":{"gc":"disabled","time":2.55,"gc_count":0,"memory":"549 MB"}}
{"2.1.5":{"gc":"enabled","time":29.1,"gc_count":0,"memory":"152 MB"}} --> memory leak
{"2.1.5":{"gc":"enabled","time":3.57,"gc_count":16,"memory":"235 MB"}}
{"2.1.5":{"gc":"enabled","time":4.19,"gc_count":10,"memory":"34 MB"}}
{"2.1.5":{"gc":"enabled","time":3.39,"gc_count":10,"memory":"0 MB"}}

// CSV
{"2.1.5":{"gc":"enabled","time":1.27,"gc_count":5,"memory":"48 MB"}}
{"2.1.5":{"gc":"enabled","time":1.15,"gc_count":2,"memory":"17 MB"}}
{"2.1.5":{"gc":"enabled","time":1.48,"gc_count":3,"memory":"3 MB"}}
{"2.1.5":{"gc":"disabled","time":1.09,"gc_count":0,"memory":"102 MB"}}
{"2.1.5":{"gc":"enabled","time":1.07,"gc_count":0,"memory":"103 MB"}} --> memory leak
{"2.1.5":{"gc":"enabled","time":0.29,"gc_count":0,"memory":"63 MB"}} --> memory leak
{"2.1.5":{"gc":"disabled","time":0.3,"gc_count":0,"memory":"64 MB"}}
{"2.1.5":{"gc":"enabled","time":0.41,"gc_count":0,"memory":"50 MB"}} --> memory leak
{"2.1.5":{"gc":"enabled","time":0.1,"gc_count":1,"memory":"6 MB"}}
{"2.1.5":{"gc":"enabled","time":0.1,"gc_count":1,"memory":"0 MB"}}

### Vikstar 1.6.3 (23 de Novembro de 2015)
* Gráficos de ABS ok;
* Determinar limite no ranking das CIDs, ok;


### Vikstar 1.6.2 (23 de Novembro de 2015)
* Página de ABS ok, resta os gráficos;

### Vikstar 1.6.1 (12 de Novembro de 2015)
* Correção de texto e usabilidade no relatório das CIDs;
* Legenda das CIDs com maior prevalência;

### Vikstar 1.6.0 (12 de Novembro de 2015)
* Criado a página de relatório das CIDs;

### Vikstar 1.5.8 (12 de Novembro de 2015)
* Corrigido as buscas: buscando pela data do atestado, ao invés da data de entrega;
* Alterado o plugin de data;
* BD PostGres;
* Layout alinhado com o sistema de palitagens;

### Vikstar 1.5.7 (11 de Novembro de 2015)

* Otimizado inserção de dados em massa
https://www.coffeepowered.net/2009/01/23/mass-inserting-data-in-rails-without-killing-your-performance/

--- Busca Relatório Sem Elastic: 3.5243219999999997 miliseg / Cache: 0.145494 miliseg
--- Busca Relatório Com Elastic: 61.241735999999996 miliseg

* Otimizado o excesso de SQLs nas buscas dos relatórios

### Vikstar 1.5.6 (10 de Novembro de 2015)
* Reduziu de 11487.9 miliseg -> 2.4 miliseg relatório de atestados: aumento de performance em 99,97%;

--- Com Rails sem transaction
Total de registros: 4093
Duplicados: 37
Registros com erros: 0
Registros cadastrados anteriormente: 0
Salvos no banco de dados: 4093
Planilha foi lida em: 4799.340198000001 miliseg
Dados foram analisados ou salvos em: 389381.376827 miliseg / 6,48min

--- Com ElasticSearch sem transaction
Total de registros: 4093
Duplicados: 37
Registros com erros: 0
Registros cadastrados anteriormente: 0
Salvos no banco de dados: 4093
Planilha foi lida em: 5006.107408 miliseg
Dados foram analisados ou salvos em: 374019.53798300005 miliseg / 6,2 min

--- Com SQL Raw transaction
Total de registros: 4093
Duplicados: 37
Registros com erros: 0
Registros cadastrados anteriormente: 0
Salvos no banco de dados: 4093
Planilha foi lida em: 3786.434483 miliseg
Dados foram analisados ou salvos em: 21022.445656 miliseg / 0,35 min

* Importação de dados aumento de performance em 94,59%;
https://www.coffeepowered.net/2009/01/23/mass-inserting-data-in-rails-without-killing-your-performance/

* Otimizado a performance do Highcharts e DataTables: http://www.highcharts.com/studies/performance.php?seriesType=line&dataLabels=on&chartCount=16&seriesPerChart=2&pointsPerSeries=16&chartWidth=250&libSource=http%3A%2F%2Fcode.highcharts.com%2Fhighcharts.js
http://stackoverflow.com/questions/14642779/highcharts-how-can-i-turn-off-the-points
http://stackoverflow.com/questions/14768021/highcharts-performance-enhancement-method

https://datatables.net/examples/ajax/defer_render.html

### Vikstar 1.5.5 (5 de Novembro de 2015)
* Relatório novo está pronto;
* Mantido a página de relatório antigo em: /sesmt/faltas_legais_old;

### Vikstar 1.5.4 (4 de Novembro de 2015)
* Refeita as interfaces;
* Concluída a importação das planilhas;

### Vikstar 1.5.3 (3 de Novembro de 2015)

* Substituindo o Foundation pelo Bootstrap

### Vikstar 1.5.2 (5 de Maio de 2015)

* Retirado os strips da matrícula, quantidade de dias e CRM devido a mudança de dados e ocasionava erros;

### Vikstar 1.5.1 (28 de Abril de 2015)

* Corrigido JS para aceitar títulos dos tipos de centros de custos com pontuação;

### Vikstar 1.5.0 (21 de Abril de 2015)

* Todos os relatórios de atestados geram Excel;

### Vikstar 1.4.0 (10 de Abril de 2015)

* Relatório de números de dias por cada CID nos centros de custo;
* Relatório de faltas legais;
* Código refatorado;
* Refeita as rotas cid -> legal_fault, pois os relatórios estão mais abrangentes e detalhados;

### Vikstar 1.3.0 (8 de Abril de 2015)

* Apresentando dados de Atestados, Declaraçoes de hora, Faltas legais, Acompanhamentos, Certidoes;
* Refatorado todo o codigo para aceitar varios tipos de atestados;

### Vikstar 1.2.0 (8 de Abril de 2015)

* Aceitando atestados com CIDs em branco;
* Validação de atestados duplicados, removendo-os. Foi criado um ID (Matricula+CID+Data de entrega+Data do atestado) pra cada atestado;
* Adicionado resumo dos dados;
* Adicionado o relatório de CRM, Matrículas;

### Vikstar 1.1.0 (7 de Abril de 2015)

* Mudei o tema para branco pois facilita na impressão e apresentação dos relatórios;
* Relatório das CIDs "Todos" em ordem crescente;
* Apresentação da quantidade dos dados sem precisar do hover do mouse. Melhor para impressão;
* Adicionado relatório de quantidade de atestados por matrícula no período;

### Vikstar 1.0.0 (28 de Outubro de 2014)

* Melhorado interface dos Boletos para apresentar o valor do total de boletos, a quantidade de boletos e o valor total da diferença;
* Apresentação do gráfico para visualizar o total de boletos por centro de custo;
